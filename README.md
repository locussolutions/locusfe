#***Locus***

*Front-End*

# Tech Stack
- **AngularJS**
- **HTML5**
- **CSS3**
- **Vanilla JS**

# Third party Libraries
- **Bootstrap CSS**
- **Bootstrap JS**

# Package Control
- **Bower**
- **Node Package Manager**

# Packaging
Involves compiling and deployment within **Cordova** and **Phonegap**
