angular.module('Locus', [
    'ngRoute',
    'mobile-angular-ui',
    'oc.lazyLoad',
    'Locus.DashController'
])
    .config(function ($routeProvider) {
    $routeProvider
        .when('/', {templateUrl: 'home.html', reloadOnSearch: false})
        .when('/login', {templateUrl: 'login.html', controller: 'LoginController', reloadOnSearch: false})
        .when('/dashboard', {
            templateUrl: 'dashboard.html',
            controller: 'DashboardController',
            controllerAs: 'dCtrl',
            reloadOnSearch: false
        });
});
