(function () {
    'use strict';

    angular.module('Locus.DashController', [])
        .controller('DashboardController', DashboardController)
        .service('WidgetService', WidgetService);

    function DashboardController($scope, $controller, $rootScope, $location, WidgetService) {
        var vm = this;

        vm.callExt = $location.absUrl();
        vm.callExt = vm.callExt.replace('http://localhost:3000/#!/', '');

        vm.allWidgets = [];
        WidgetService.fetchAssociatedWidgets(vm.callExt).then(function (data) {
            vm.allWidgets = data;
        });


        $scope.getDataFromDataUrl = function (dataUrl) {
            WidgetService.fetchDataFromUrl(dataUrl).then(function (data) {
                vm.dataResponseSet = data;
            });
        }

    }

    function WidgetService($http) {
        var vm = this;
        vm.fetchAssociatedWidgets = function (pageUrl) {
            return $http({
                method: 'GET',
                url: 'http://localhost:8000/widget-manager/widgets/',
                params: {
                    'hitUrl': pageUrl
                },
                header: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function (response) {
                return response.data;
            });
        };

        vm.fetchDataFromUrl = function (dataExt) {
            return $http({
                method: 'GET',
                url: 'http://localhost:8000/' + dataExt,
                params: {},
                header: {'Content-Type': 'application/x-www-form-urlencoded'}
            }).then(function (response) {
                return response.data;
            });
        }
    }

}());
