angular
    .module('Locus.DashController', [])
    .controller('LoginController', LoginController);

function LoginController($scope, $controller, $rootScope, $http, $location) {
    var vm = this;
    vm.stop = $location.absUrl();
    vm.kill = vm.stop.replace('http://localhost:3000/#!/', '');

    function callData() {
        return $http({
            method: 'GET',
            url: 'http://localhost:8000/widget-manager/widgets/',
            params: {
                'hitUrl': "dashboard"
            },
            header: {'Content-Type': 'application/x-www-form-urlencoded'}
        });
    }

    callData().then(function(data) {
        console.log(data);
    })
}
